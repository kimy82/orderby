import * as chai from 'chai';
import * as mocha from 'mocha';

var expect: Chai.ExpectStatic = chai.expect;

import OrderBy from '../orderby';

describe('Order By', function () {

    const persons = [{name: 'name', age: 1},{name: 'name', age: 13},{name: 'name', age: 2},{name: 'name', age: 0}];

    it('Should order by age ASC', function (done) {
        const orderedPersons = OrderBy.age(persons, 'ASC');
        expect(orderedPersons[0].age).to.equal(0);
        expect(orderedPersons[1].age).to.equal(1);
        expect(orderedPersons[2].age).to.equal(2);
        expect(orderedPersons[3].age).to.equal(13);
        done();
    });

    it('Should order by age DESC', function (done) {
        const orderedPersons = OrderBy.age(persons, 'DESC');
        expect(orderedPersons[0].age).to.equal(13);
        expect(orderedPersons[1].age).to.equal(2);
        expect(orderedPersons[2].age).to.equal(1);
        expect(orderedPersons[3].age).to.equal(0);
        done();
    });

});