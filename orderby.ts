interface HotjarPerson {
    name: string;
    age: number;
}

type UpOrDown = 'ASC' | 'DESC';

class Orderby {

    /**
     * Order the array by Age.
     * 
     * @param array the array to order.
     * @param upOrDown ASC / DESC
     */
    public static age(array: HotjarPerson[], upOrDown: UpOrDown): HotjarPerson[] {
        return array.sort((hotjarPerson1: HotjarPerson, hotjarPerson2: HotjarPerson) => {
            let result = hotjarPerson1.age > hotjarPerson2.age;
            if (upOrDown == 'DESC') result = !result;
            return result ? 1 : 0;
        });
    };
}

export default Orderby;